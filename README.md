# WordSearch

Simple search engine (Only tested with Ruby 2.4)

## Installation

Pull the repo from `https://bitbucket.org/mustafaturan/word_search` and run from your console, inside `word_search` root directory:

```shell
chmod +x bin/app.rb
bin/app.rb
```

## Usage

When you run the app, it will come with the following commands:

`/q` to quit

`/h` to see `search` help

`/s 'schema name' 'search field' 'search terms'` to search

`/l` to list searchable fields

## Development

### Directory structure

Source code runs under `/lib` folder:

```
+ /word_search
  + /collections # List of collections related to JSON DB files
  + /plugs # Includes 'indexable', 'loadable', and 'searchable' extensions for collections. Collection extensions can be made via modules.
  + /services # Helper services
  + /views # Console printing layer; folder includes view files
  + /console.rb # it controls console IO
  + /version.rb # it only includes version info

```

### Adding a new DB

Databases can be found under `/db` folder.

1 - Add your json DB file under `/db` folder

2 - Create a new collection with the same filename(without ext).

3 - Add your new collection to `word_search.rb` file.

4 - If you need to serve this DB on command line app then add it to searchables hash in `console.rb`

### Methodology

It creates a simple hash to read the data as fast as possible with O(1) complexity. It simply creates a hash-key for every word and add doc indices as item to the hash value. Current implementation creates one hash per table.

### Tools

Rubocop (code quality)

Simplecov (test coverage)

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/mustafaturan/word_search . (Make sure you included your test.)

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
