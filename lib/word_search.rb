require File.dirname(__FILE__) + '/word_search/version'
require File.dirname(__FILE__) + '/word_search/plugs/loadable'
require File.dirname(__FILE__) + '/word_search/plugs/indexable'
require File.dirname(__FILE__) + '/word_search/plugs/searchable'
require File.dirname(__FILE__) + '/word_search/services/nlp'
require File.dirname(__FILE__) + '/word_search/collections/collection_base'
require File.dirname(__FILE__) + '/word_search/collections/user'
require File.dirname(__FILE__) + '/word_search/collections/organization'
require File.dirname(__FILE__) + '/word_search/collections/ticket'
require File.dirname(__FILE__) + '/word_search/views/welcome'
require File.dirname(__FILE__) + '/word_search/views/search_fields'
require File.dirname(__FILE__) + '/word_search/views/search_results'
require File.dirname(__FILE__) + '/word_search/views/search_usage'
require File.dirname(__FILE__) + '/word_search/console'

# Simple Word Search
module WordSearch
end
