module WordSearch
  # Console runnable
  class Console
    attr_reader :searchables

    def initialize
      @searchables = {
        'organizations' => WordSearch::Organization.new.load_docs.index_docs,
        'tickets' => WordSearch::Ticket.new.load_docs.index_docs,
        'users' => WordSearch::User.new.load_docs.index_docs
      }

      # print welcome screen
      WordSearch::View::Welcome.print
    end

    def run
      # loop for console commands
      loop do
        input = gets.strip!
        command, *params = "#{input} ".split(/\s/)
        cont, _res = run_cmd(command, params)
        break unless cont
      end
    end

    private

    def run_cmd(cmd, params) # rubocop:disable Metrics/MethodLength
      case cmd
      when '/h'
        [true, help]
      when '/s'
        [true, search(params)]
      when '/l'
        [true, search_fields]
      when '/q'
        [false, puts('Bye!')]
      else
        [true, puts('Invalid command!')]
      end
    rescue StandardError => ex
      [true, puts("ERROR\n#{ex.message}")]
    end

    def help
      WordSearch::View::SearchUsage.print
    end

    def search_fields
      WordSearch::View::SearchFields.print(@searchables)
    end

    def search(params)
      if params.count < 2
        raise IOError, 'Missing parameter; type /h to see search help screen'
      end
      search_with_valid_params(params)
    end

    def search_with_valid_params(params)
      params.reverse!
      table_name = params.pop.downcase
      model = @searchables[table_name] || raise(StandardError, 'Invalid schema')
      search_key = params.pop
      search_terms = params.join(' ')
      docs = model.search(search_key, search_terms)
      WordSearch::View::SearchResults.print(table_name, search_key,
                                            search_terms, docs)
    end
  end
end
