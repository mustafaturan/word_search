module WordSearch
  module Service
    # NLP service
    class NLP
      class << self
        def extract_terms(str)
          if str.is_a?(Array)
            str
          else
            search_terms = str.to_s.strip.gsub(/([\n\r\t\s]+)/, ' ').split(' ')
            search_terms == [] ? [nil] : search_terms # for blank vals
          end
        end
      end
    end
  end
end
