module WordSearch
  module Plug
    # Searchable plug
    module Searchable
      def search(search_key, str)
        validate_search_key_existence(search_key)
        search_terms = WordSearch::Service::NLP.extract_terms(str)
        doc_indices = filter_doc_indices(search_key, search_terms)
        doc_indices.map { |index| @docs[index] }
      end

      def searchable_keys
        @indices.keys
      end

      private

      def filter_doc_indices(search_key, search_terms)
        found_doc_indices = []
        search_terms.each do |search_term|
          found_docs = @indices[search_key][search_term] || []
          found_doc_indices << found_docs
        end
        found_doc_indices.inject(:&)
      end

      def validate_search_key_existence(search_key)
        raise IOError, 'Search key not found!' unless @indices.key?(search_key)
      end
    end
  end
end
