module WordSearch
  module Plug
    # Indexable plug
    module Indexable
      def index_docs
        @indices = {}
        @docs.each_with_index do |doc, doc_index|
          add_to_index(doc, doc_index)
        end
        self
      end

      private

      def add_to_index(doc, doc_index)
        doc.each do |search_key, val|
          search_terms = WordSearch::Service::NLP.extract_terms(val)
          push_doc_index_to_search_terms(doc_index, search_key, search_terms)
        end
      end

      def push_doc_index_to_search_terms(doc_index, search_key, search_terms)
        @indices[search_key] ||= {}
        search_terms.each do |search_term|
          push_doc_index_to_search_term(doc_index, search_key, search_term)
        end
      end

      def push_doc_index_to_search_term(doc_index, search_key, search_term)
        @indices[search_key][search_term] ||= []
        @indices[search_key][search_term].push(doc_index)
      end
    end
  end
end
