module WordSearch
  module Plug
    # Loadable plug
    module Loadable
      require 'json'

      def load_docs(path = nil)
        document = read(path || db_path)
        json_parse(document)
        self
      end

      private

      def read(file_path)
        File.read(file_path)
      rescue
        raise IOError, 'Error occured while reading from the given path: '\
                       "`#{file_path}`"
      end

      def json_parse(document)
        @docs = JSON.parse(document)
      rescue
        raise IOError, 'Unprocessable entity!'
      end

      def db_path
        raise IOError, 'You have to implement either `db_path` or '\
                       'pass `path` as an argument'
      end
    end
  end
end
