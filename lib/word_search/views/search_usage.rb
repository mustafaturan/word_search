module WordSearch
  module View
    # Printer for search usage help
    class SearchUsage
      class << self
        DASH_LINE = ('-' * 80).freeze

        def print
          puts "Search usage:\n"\
               "/s 'schema name' 'search field' 'search terms'\n"\
               "Example: To find tickets with _id field 1\n"\
               "/s Tickets _id 1\n\n"
        end
      end
    end
  end
end
