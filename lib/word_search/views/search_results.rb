module WordSearch
  module View
    # Printer for search results
    class SearchResults
      class << self
        DASH_LINE = ('-' * 80).freeze
        MAX_KEY_LEN = 36

        def print(table_name, search_key, search_term, docs)
          put_header(table_name, search_key, search_term)
          docs.count.zero? ? put_not_found : put_docs(docs)
        end

        ## PRIVATE

        def put_header(table_name, search_key, search_term)
          puts "Searching '#{table_name.downcase}' for '#{search_key}' with a "\
               "value of '#{search_term}'\n#{DASH_LINE}"
        end

        def put_not_found
          puts 'No results found'
        end

        def put_doc_header
          puts "#{add_suffix_space('Field')}\tValue\n#{DASH_LINE}"
        end

        def put_docs(docs)
          docs.each do |doc|
            put_doc_header
            put_doc(doc)
            puts "\n\n"
          end
        end

        def put_doc(doc)
          doc.each do |key, val|
            puts "#{add_suffix_space(key)}\t#{val}"
          end
        end

        # Little string manipulation to fix tabular view
        def add_suffix_space(str)
          str + (' ' * (MAX_KEY_LEN - str.length))
        end
      end

      private_class_method :add_suffix_space, :put_doc, :put_docs,
                           :put_not_found, :put_header
    end
  end
end
