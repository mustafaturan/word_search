module WordSearch
  module View
    # Printer for search fields
    class SearchFields
      class << self
        DASH_LINE = ('-' * 80).freeze

        def print(searchables)
          searchables.each do |key, searchable|
            puts DASH_LINE
            put_searchable_header(key)
            put_searchable_keys(searchable.searchable_keys)
            puts "\n\n"
          end
        end

        ## PRIVATE

        def put_searchable_header(table_name)
          puts "Search '#{table_name}' with fields:"
        end

        def put_searchable_keys(keys)
          keys.each do |key|
            puts key
          end
        end
      end

      private_class_method :put_searchable_keys, :put_searchable_header
    end
  end
end
