module WordSearch
  module View
    # Printer for welcome message
    class Welcome
      class << self
        DASH_LINE = ('-' * 80).freeze

        def print
          puts "Welcome to Simple Word Search\n\n"\
               "Available commands\n"\
               "#{DASH_LINE}\n"\
               "Type `/q` to quit\n"\
               "Type `/h` to see `search` help\n"\
               "Type `/s 'schema name' 'search field' 'search terms'` to "\
               "search\n"\
               "Type `/l` to list searchable fields\n\n\n"
        end
      end
    end
  end
end
