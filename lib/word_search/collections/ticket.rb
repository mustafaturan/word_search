module WordSearch
  # Ticket collection
  class Ticket < WordSearch::CollectionBase
    def initialize
      @name = 'Tickets'.freeze
      super
    end
  end
end
