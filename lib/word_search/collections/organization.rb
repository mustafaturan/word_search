module WordSearch
  # Organization collection
  class Organization < WordSearch::CollectionBase
    def initialize
      @name = 'Organizations'.freeze
      super
    end
  end
end
