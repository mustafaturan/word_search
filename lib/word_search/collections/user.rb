module WordSearch
  # User collection
  class User < WordSearch::CollectionBase
    def initialize
      @name = 'Users'.freeze
      super
    end
  end
end
