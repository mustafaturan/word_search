module WordSearch
  # Base collection
  class CollectionBase
    attr_reader :name, :docs, :indices

    include WordSearch::Plug::Loadable
    include WordSearch::Plug::Indexable
    include WordSearch::Plug::Searchable

    def initialize
      @name ||= ''
      @docs = []
      @indices = {}
    end

    private

    def db_path
      @db_path ||= File.expand_path("../../../db/#{name.downcase}.json",
                                    File.dirname(__FILE__))
    end
  end
end
