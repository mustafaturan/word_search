#!/usr/bin/env ruby

require File.expand_path('../lib/word_search', File.dirname(__FILE__))

begin
  WordSearch::Console.new.run
rescue StandardError => ex
  puts ex.message
  exit
end
