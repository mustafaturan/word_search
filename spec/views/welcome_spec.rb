require 'spec_helper'

RSpec.describe WordSearch::View::Welcome do
  let(:printer) { WordSearch::View::Welcome }

  it '.print' do
    expect do
      printer.print
    end.to output(/(Welcome)/).to_stdout
  end
end
