require 'spec_helper'

RSpec.describe WordSearch::View::SearchResults do
  describe '.print' do
    let(:table_name) { 'users' }
    let(:search_key) { 'title' }
    let(:search_term) { 'Hey' }
    let(:printer) { WordSearch::View::SearchResults }

    it 'with empty doc list' do
      docs = []
      expect do
        printer.print(table_name, search_key, search_term, docs)
      end.to output(/No results found/).to_stdout
    end

    it 'with non-empty doc list' do
      docs = [
        {'_id' => '123456', 'title' => 'Hey'},
        {'_id' => '123567', 'title' => 'Hey'}
      ]

      expect do
        printer.print(table_name, search_key, search_term, docs)
      end.to output(/(123456)/).to_stdout

      expect do
        printer.print(table_name, search_key, search_term, docs)
      end.to output(/(123567)/).to_stdout

      expect do
        printer.print(table_name, search_key, search_term, docs)
      end.not_to output(/No results found/).to_stdout
    end
  end
end
