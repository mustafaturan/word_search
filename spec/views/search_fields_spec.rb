require 'spec_helper'

RSpec.describe WordSearch::View::SearchFields do
  let(:printer) { WordSearch::View::SearchFields }

  class SampleSearchFields
    attr_reader :searchable_keys

    def initialize(searchable_keys)
      @searchable_keys = searchable_keys
    end
  end

  it '.print' do
    searchables = {
      'users' => SampleSearchFields.new([:id, :fullname]),
      'posts' => SampleSearchFields.new([:id, :title, :body]),
    }

    expect do
      printer.print(searchables)
    end.to output(/(users)/).to_stdout

    expect do
      printer.print(searchables)
    end.to output(/(posts)/).to_stdout

    expect do
      printer.print(searchables)
    end.to output(/(id\nfullname)/).to_stdout

    expect do
      printer.print(searchables)
    end.to output(/id\ntitle\nbody/).to_stdout
  end
end
