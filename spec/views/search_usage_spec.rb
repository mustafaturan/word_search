require 'spec_helper'

RSpec.describe WordSearch::View::SearchUsage do
  let(:printer) { WordSearch::View::SearchUsage }

  it '.print' do
    expect do
      printer.print
    end.to output(/(Search usage)/).to_stdout
  end
end
