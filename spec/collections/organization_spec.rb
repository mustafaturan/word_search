require 'spec_helper'

RSpec.describe WordSearch::Organization do
  let(:organization) { WordSearch::Organization.new() }

  it '#name returns `Organizations`' do
    expect(organization.name).to eq('Organizations')
  end

  it '#db_path returns correct path' do
    expected_path = File.expand_path("../../db/organizations.json",
      File.dirname(__FILE__))
    expect(organization.send(:db_path)).to eq(expected_path)
  end
end
