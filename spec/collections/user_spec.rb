require 'spec_helper'

RSpec.describe WordSearch::User do
  let(:user) { WordSearch::User.new() }

  it '#name returns `users`' do
    expect(user.name).to eq('Users')
  end

  it '#db_path returns correct path' do
    expected_path = File.expand_path("../../db/users.json",
      File.dirname(__FILE__))
    expect(user.send(:db_path)).to eq(expected_path)
  end
end
