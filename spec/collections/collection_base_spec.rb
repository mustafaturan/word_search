require 'spec_helper'

RSpec.describe WordSearch::CollectionBase do
  let(:collection) { WordSearch::CollectionBase.new }

  it '#initialize' do
    expect(collection.name).to eq('')
    expect(collection.docs).to eq([])
    expect(collection.indices).to eq({})
  end

  it '#load_docs method exists' do
    expect(collection).to respond_to(:load_docs)
  end

  it '#index_docs method exists' do
    expect(collection).to respond_to(:index_docs)
  end

  it '#search method exists' do
    expect(collection).to respond_to(:search)
  end

  it '#search method exists' do
    expect(collection).to respond_to(:search)
  end

  it '#searchable_keys method exists' do
    expect(collection).to respond_to(:searchable_keys)
  end
end
