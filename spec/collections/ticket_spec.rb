require 'spec_helper'

RSpec.describe WordSearch::Ticket do
  let(:ticket) { WordSearch::Ticket.new() }

  it '#name returns `Tickets`' do
    expect(ticket.name).to eq('Tickets')
  end

  it '#db_path returns correct path' do
    expected_path = File.expand_path("../../db/tickets.json",
      File.dirname(__FILE__))
    expect(ticket.send(:db_path)).to eq(expected_path)
  end
end
