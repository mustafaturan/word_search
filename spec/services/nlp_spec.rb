require 'spec_helper'

RSpec.describe WordSearch::Service::NLP do
  let(:nlp) { WordSearch::Service::NLP }

  describe '.extract_terms' do
    it 'with array' do
      expected_terms = ['slicon', 'valley']
      expect(nlp.extract_terms(expected_terms)).to eq(expected_terms)
    end

    it 'with nil' do
      expected_terms = [nil]
      expect(nlp.extract_terms('')).to eq(expected_terms)
    end

    it 'with blank' do
      expected_terms = [nil]
      expect(nlp.extract_terms('')).to eq(expected_terms)
    end

    it 'with one word' do
      expected_terms = ['slicon']
      expect(nlp.extract_terms('slicon')).to eq(expected_terms)
    end

    it 'with multiple words with spaces' do
      expected_terms = ['slicon', 'valley', 'buzz']
      expect(nlp.extract_terms('  slicon   valley buzz ')).to eq(expected_terms)
    end
  end
end
