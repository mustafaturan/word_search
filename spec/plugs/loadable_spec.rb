require 'spec_helper'

RSpec.describe WordSearch::Plug::Loadable do
  class LoadSample
    include WordSearch::Plug::Loadable

    attr_reader :docs
  end

  let(:sample) { LoadSample.new }

  it '#load_docs' do
    file_path = File.expand_path("../fixtures/users.json",
      File.dirname(__FILE__))
    sample.load_docs(file_path)
    expect(sample.docs.count).to eq(3)
  end

  it '#load_docs raises when path is not given' do
    expect { sample.load_docs() }.to raise_error(IOError)
  end
end
