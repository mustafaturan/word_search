require 'spec_helper'

RSpec.describe WordSearch::Plug::Searchable do
  class SearchSample
    include WordSearch::Plug::Indexable
    include WordSearch::Plug::Searchable

    attr_reader :docs, :indices

    def initialize
      @docs = [
        {'id' => 1, 'name' => 'Mustafa', 'surname' => 'Turan', 'desc' => ''},
        {'id' => 2, 'name' => 'Gozde', 'surname' => 'Turan', 'desc' => 'Lovely'}
      ]
    end
  end

  let(:sample) { SearchSample.new }

  describe '#search' do
    before do
      sample.index_docs
    end

    it 'with unique field' do
      docs = sample.search('id', 1)
      expect(docs.count).to eq(1)
      expect(docs.first['id']).to eq(1)
    end

    it 'with common field' do
      docs = sample.search('surname', 'Turan')
      expect(docs.count).to eq(2)
      expect(docs.first['id']).to eq(1)
      expect(docs.last['id']).to eq(2)
    end

    it 'with blank field' do
      docs = sample.search('desc', '')
      expect(docs.count).to eq(1)
      expect(docs.first['id']).to eq(1)
    end
  end
end
