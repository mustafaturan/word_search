require 'spec_helper'

RSpec.describe WordSearch::Plug::Indexable do
  class IndexSample
    include WordSearch::Plug::Indexable

    attr_reader :docs, :indices

    def initialize
      @docs = [{id: 1, name: 'Mustafa', surname: 'Turan'},
        {id: 2, name: 'Gozde', surname: 'Turan'}]
    end
  end

  let(:sample) { IndexSample.new }

  it '#index_docs' do
    expected_indices = {:id=>{'1'=>[0], '2'=>[1]}, :name=>{'Mustafa'=>[0],
      'Gozde'=>[1]}, :surname=>{'Turan'=>[0, 1]}}
    sample.index_docs
    expect(sample.indices).to eq(expected_indices)
  end
end
